package com.epam;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private static Scanner input = new Scanner(System.in);
    MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - input the coefficients of the quadratic equation");
        menu.put("2", "  2 - get discriminator");
        menu.put("3", "  3 - get the roots of the quadratic equation");
        menu.put("QUIT(Q)", "  QUIT(Q) - exit");
    }
    public void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("Please, select menu point:");
    }
    public void output(String nameOfOutput, double discriminator) {
        System.out.println(nameOfOutput + " = " + discriminator);
    }
    public void outputMessage(String message) {
        System.out.println(message);
    }
    public String input() {
        return input.nextLine().toUpperCase();
    }
    public double getCoefficients(String nameOfCoefficients) {
        System.out.println("Input " + nameOfCoefficients);
        return input.nextDouble();
    }
}
