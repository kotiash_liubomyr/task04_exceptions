package com.epam;

@FunctionalInterface
public interface Printable {

    void print(QuadraticEquation quadraticEquation);
}
