package com.epam;

import java.util.LinkedHashMap;
import java.util.Map;

public class Controler {
    private MyView myView;
    private Map<String, Printable> methodsMenu;
    Controler() {
        myView = new MyView();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::inputCoefficients);
        methodsMenu.put("2", this::getDiscriminator);
        methodsMenu.put("3", this::getRoots);
    }
    public void start() {
        try (QuadraticEquation quadraticEquation = new QuadraticEquation(1, 1, 1)) {
            String command;
            do {
                myView.outputMenu();
                command = myView.input();
                if (methodsMenu.containsKey(command)) {
                    methodsMenu.get(command).print(quadraticEquation);
                }
            } while (!command.equals("QUIT") && !command.equals("Q"));
            return;
        } catch (Exception e) {
            myView.outputMessage(e.getMessage());
        }
    }
    private void inputCoefficients(QuadraticEquation quadraticEquation) {
        double a = myView.getCoefficients("a");
        double b = myView.getCoefficients("b");
        double c = myView.getCoefficients("c");
        quadraticEquation.copy(new QuadraticEquation(a, b, c));
    }
    private void getDiscriminator(QuadraticEquation quadraticEquation) {
        if(quadraticEquation != null) {
            myView.output("D", quadraticEquation.getD());
        } else {
            myView.outputMessage("You must input the coefficients of the quadratic equation!!!");
        }
    }
    private void getRoots(QuadraticEquation quadraticEquation) {
        if(quadraticEquation != null) {
            myView.output("x1", quadraticEquation.getX1());
            myView.output("x2", quadraticEquation.getX2());
        } else {
            myView.outputMessage("You must input the coefficients of the quadratic equation!!!");
        }
    }
}
