package com.epam;

public class QuadraticEquation implements AutoCloseable {
    private double a;
    private double b;
    private double c;
    QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public void copy(QuadraticEquation quadraticEquation) {
        this.a = quadraticEquation.a;
        this.b = quadraticEquation.b;
        this.c = quadraticEquation.c;
    }
    double getD() {
        return b * b - 4 * a * c;
    }
    double getX1() {
        return (-b + Math.sqrt(getD())) / 2 / a;
    }
    double getX2() {
        return (-b - Math.sqrt(getD())) / 2 / a;
    }
    @Override
    public void close() throws Exception {
        System.err.println("QuadraticEquation has closed!!!");
    }
}
